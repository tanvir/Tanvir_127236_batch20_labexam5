<?php
session_start();
include_once ('../../../vendor/autoload.php');
//var_dump($_POST);


use App\Bitm\SEIP127236\Student\Student;
use App\Bitm\SEIP127236\Student\Message;
$student = new Student();
$allInfo=$student->index();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Student Info</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

</head>


<h2> All Students' Name </h2><hr><br>

<button onclick="window.location.href='create.php'"> Add Student Name </button> <br><br>

<div id="message">
    <?php
    if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
        echo Message::message();
    }
    ?>
</div>
<table style="width:40%">
    <tr>
        <th> Sl no. </th>
        <th> ID </th>
        <th> First Name </th>
        <th> Middle Name </th>
        <th> Last Name </th>
        <th> Action </th>
    </tr>
    <?php
    $sl=0;
    foreach ($allInfo as $name){
        $sl++?>
        <tr>
            <td><?php echo $sl?></td>
            <td><?php echo $name->id ?></td>
            <td><?php echo $name->firstname ?></td>
            <td><?php echo $name->middlename ?></td>
            <td><?php echo $name->lastname ?></td>
            <td><button id="id3" onclick="window.location.href='view.php?id=<?php echo $name->id ?>'"> View </button>
                <button id="id4" onclick="window.location.href='edit.php?id=<?php echo $name->id ?>'"> Edit </button>
                <button id="id5" onclick="window.location.href='delete.php?id=<?php echo $name->id ?>'"> Delete </button>

            </td>
        </tr>
    <?php }?>
</table>

<script>
    $('#message').show().delay(5000).fadeOut();
</script>

</body>
</html>

